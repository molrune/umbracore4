#include "ScriptPCH.h"

class npc_helpme : public CreatureScript
{
	public : 
		npc_helpme() : CreatureScript("npc_helpme") {}

	struct npc_helpmeAI : public ScriptedAI
	{
		npc_helpmeAI(Creature* c) : ScriptedAI(c) {}
		
		uint32 speak_timer;

		void UpdateAI(uint32 diff)
		{
			if (speak_timer <= diff)
			{
				me->MonsterSay("...Oh! Help Me!...", LANG_UNIVERSAL, 0); // Change this to change what NPC says
				speak_timer = 60000; // Changed to 1 Min
			} else speak_timer -= diff;
		}
		
		void Reset()
		{
			speak_timer = 60000; // Changed to 1 Min 
		}
	};
	
	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_helpmeAI(creature);
	}
};

void AddSC_npc_helpme()
{
	new npc_helpme();
}