# Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

set(scripts_STAT_SRCS
  ${scripts_STAT_SRCS}
  ############################
  #    UmbraCore4 Custom     # 
  ############################
  custom/Beastmaster.cpp
  custom/BountyHunter.cpp
  custom/item_buff.cpp
  custom/npc_enchantment.cpp
  custom/boss_emoriss.cpp
  custom/chat.cpp
  custom/npc_server_helper.cpp
  custom/vipcommands.cpp
  #custom/npcs_dm_battle.h
  #custom/npcs_dm_battle.cpp
  custom/duel_reset.cpp
  custom/npc_premium_master.cpp
  custom/gomove.cpp
  custom/antiadd.cpp
  custom/professions.cpp
  custom/customizer.cpp
  custom/trainerspawn.cpp
  custom/Reforging.cpp
    
  #############################
  #  Simple NPC In-Game Text  # 
  #############################
  custom/npc_say_timmy.cpp
  custom/npc_say_resist.cpp
  custom/npc_say_helpme.cpp
  
  ##############################
  #     Transmogrification     #
  ##############################
  custom/TransmogEngine.cpp
  custom/TransmogEngine.h
  custom/TransmogHandler.cpp
  custom/TransmogMgr.cpp
  custom/TransmogMgr.h
  
  ############################
  #         Levelers         #
  ############################
  custom/Levelers/leveler.cpp             
  custom/Levelers/leveler_10.cpp
  custom/Levelers/leveler_20.cpp
  custom/Levelers/leveler_30.cpp
  custom/Levelers/leveler_40.cpp
  custom/Levelers/leveler_50.cpp
  custom/Levelers/leveler_60.cpp
  custom/Levelers/leveler_70.cpp
  custom/Levelers/leveler_80.cpp
  custom/Levelers/leveler_90.cpp
  custom/Levelers/leveler_100.cpp
  custom/Levelers/leveler_110.cpp
  custom/Levelers/leveler_120.cpp
  custom/Levelers/leveler_130.cpp
  custom/Levelers/leveler_140.cpp
  custom/Levelers/leveler_150.cpp
  custom/Levelers/leveler_160.cpp
  custom/Levelers/leveler_170.cpp
  custom/Levelers/leveler_180.cpp
  custom/Levelers/leveler_190.cpp
  custom/Levelers/leveler_200.cpp
  custom/Levelers/leveler_210.cpp
  custom/Levelers/leveler_220.cpp
  custom/Levelers/leveler_230.cpp
  custom/Levelers/leveler_240.cpp
  custom/Levelers/leveler_250.cpp
  
  ##########################
  #   Individual XP Rates  #
  ##########################
  custom/custom_rates.cpp
  
  #########################
  #     Guild Houseing    #
  #########################
  custom/Guildhouse.cpp
  
  
  ##################################################################################################################
  ##################################################################################################################
  
  ############################
  #   Disabled for Re-Write  #
  ############################
  #Custom/resetduel.cpp
  #Custom/killsystem.cpp
  #Custom/arenavendor.cpp
  #Custom/OnScripts/onlogin.cpp
  #Custom/OnScripts/OnEnterMap.cpp
  #Custom/huntervendor.cpp
  #Custom/votenpc.cpp
  #Custom/donornpc.cpp
  #Custom/OnScripts/onpvpkill.cpp
  #Custom/tokenconverter.cpp
  #Custom/warmage.cpp
  #Custom/graveyard.cpp
  #Custom/battleground_spawner.cpp
  #Custom/instances.cpp
  #Custom/OnScripts/OnCreatureKill.cpp
  #Custom/PlayerHousing/player_housing.cpp
  #Custom/PlayerHousing/player_housing_npc.cpp
  #Custom/Arena_Ranks.cpp
  #Custom/transfer.cpp
  #Custom/gurubashi_theme.cpp
  #Custom/Professions_NPC.cpp
  #custom/zombie_event.cpp
  #custom/TowerDefense/TowerDefense.cpp
  #custom/TowerDefense/TowerDefense.h
  #custom/TowerDefense/TowerDefenseInstance
  #custom/ArenaGambler/ArenaGambler.cpp
  #custom/ArenaGambler/ArenaGamblingSystem.h
  #custom/ArenaGambler/ArenaGamblingSystem.cpp  
)

message("  -> Prepared: Custom")
