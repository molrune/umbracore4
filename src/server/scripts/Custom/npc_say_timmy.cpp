#include "ScriptPCH.h"

class npc_tim : public CreatureScript
{
	public : 
		npc_tim() : CreatureScript("npc_tim") {}

	struct npc_timAI : public ScriptedAI
	{
		npc_timAI(Creature* c) : ScriptedAI(c) {}
		
		uint32 speak_timer;

		void UpdateAI(uint32 diff)
		{
			if (speak_timer <= diff)
			{
				me->MonsterSay("Arg...It Hurts!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
				speak_timer = 60000; // Changed to 1 Min
			} else speak_timer -= diff;
		}
		
		void Reset()
		{
			speak_timer = 60000; // Changed to 1 Min 
		}
	};
	
	CreatureAI* GetAI(Creature* creature) const
	{
		return new npc_timAI(creature);
	}
};

void AddSC_npc_tim()
{
	new npc_tim();
}