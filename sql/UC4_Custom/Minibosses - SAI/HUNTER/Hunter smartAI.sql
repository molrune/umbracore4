-- Hunter
SET @ENTRY := 80004;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,11,75,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"On aggro do auto-attack."),
(@ENTRY,@SOURCETYPE,1,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,2,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,3,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,4,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,5,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,6,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,7,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,8,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,9,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,10,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,11,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,12,0,4,0,100,0,0,0,0,0,75,35786,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Attack damage buff due to error with range npc."),
(@ENTRY,@SOURCETYPE,13,0,0,0,100,0,30000,30000,30000,30000,11,72268,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Ice Shot(72268) every 30 seconds."),
(@ENTRY,@SOURCETYPE,14,0,0,0,100,0,40000,40000,40000,40000,75,66095,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Lightning Arrows(66095) every 40 seconds."),
(@ENTRY,@SOURCETYPE,15,0,0,0,100,0,20000,20000,20000,20000,11,40872,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Immolation Arrow(40872) every 20 seconds."),
(@ENTRY,@SOURCETYPE,16,0,0,0,100,0,6000,6000,6000,6000,11,59243,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Aimed Shot(59243) every 6 seconds."),
(@ENTRY,@SOURCETYPE,17,0,4,0,100,0,0,0,0,0,12,4,4,5000,0,1,0,0,0,0,0,0.0,0.0,0.0,0.0,"On aggro summon Beaky(4)."),
(@ENTRY,@SOURCETYPE,18,0,0,0,100,0,4000,4000,4000,4000,11,36623,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Arcane Shot(36623) every 4 seconds."),
(@ENTRY,@SOURCETYPE,19,0,4,0,100,0,0,0,0,0,11,53338,2,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Hunter�s Mark(53338)."),
(@ENTRY,@SOURCETYPE,20,0,12,0,100,0,1,20,1,1,11,61006,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"When target has 20% cast Kill Shot(61006)."),
(@ENTRY,@SOURCETYPE,21,0,13,0,100,0,1,1,0,0,11,3034,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"When target is casting cast Viper Sting(3034)"),
(@ENTRY,@SOURCETYPE,22,0,0,0,100,0,60000,60000,60000,60000,11,3045,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Rapid Fire(3045) every 60 seconds."),
(@ENTRY,@SOURCETYPE,23,0,2,0,100,1,24,25,1,1,11,19263,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 25% cast Deterrence(19263)."),
(@ENTRY,@SOURCETYPE,24,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,25,0,1,0,100,1,10000,10000,10000,10000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat.");