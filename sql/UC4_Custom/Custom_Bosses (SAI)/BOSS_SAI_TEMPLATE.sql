--SIMPLE Boss SAI Template
--UmbraCore2 (C) 2013
--Nobody

REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (NPC_ID_HERE, 0, 0, 0, 0, 0, 24942, 0, 0, 0, 'For Ac-web', 'By <Koki>', '', 0, 82, 82, 2, 16, 16, 0, 1, 1.5, 6, 3, 2000, 2480, 0, 300, 6, 2000, 0, 1, 0, 2048, 8, 0, 0, 0, 0, 0, 21, 30, 4, 4, 0, 0, 0, 0, 50, 50, 50, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5000000, 5000000, 'SmartAI', 0, 1, 1, 115, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 618348543, 0, '', 12340);

DELETE FROM `creature_text` WHERE `entry`=NPC_ID_HERE;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (NPC_ID_HERE,1,0,'Interlopers! You mortals who dare to interfere with my sport will pay... Wait--you... I remember you...',14,0,100,1,0,17457,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (NPC_ID_HERE,2,0,'You mortals......you cant...........KILL ME!!',14,0,100,1,0,17458,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (NPC_ID_HERE,3,0,'You are not prepared!',14,0,100,1,0,17459,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (NPC_ID_HERE,4,0,'NO! NO! NO! NO! NO!......Impossible!!',14,0,100,1,0,17460,'Comment');

SET @ENTRY := NPC_ID_HERE;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,2,0,100,0,90,100,6000,7000,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,2,0,2,0,100,0,70,80,10000,13000,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,3,0,2,0,100,0,60,70,0,0,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,4,0,2,0,100,0,50,60,6000,7000,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,5,0,2,0,100,1,40,50,6000,7000,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,7,0,2,0,100,0,20,30,6000,7000,11,Spell_ID_HERE,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,8,0,2,0,100,0,10,20,6000,7000,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,10,0,2,0,100,1,1,10,0,0,11,Spell_ID_HERE,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,11,0,2,0,100,0,80,90,10000,13000,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,1,30,40,8000,9000,11,Spell_ID_HERE,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,13,0,2,0,100,0,1,10,8000,9000,11,Spell_ID_HERE,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"SPELL_NAME_HERE"),
(@ENTRY,@SOURCETYPE,14,0,4,0,100,1,0,0,0,0,1,1,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 1"),
(@ENTRY,@SOURCETYPE,15,0,2,0,100,1,40,50,0,0,1,2,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 2"),
(@ENTRY,@SOURCETYPE,16,0,2,0,100,1,20,30,0,0,1,3,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 3"),
(@ENTRY,@SOURCETYPE,17,0,2,0,100,1,1,5,0,0,1,4,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 4");