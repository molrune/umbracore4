-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 668, "Language Common"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 668, "Language Common");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 668, "Language Common"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 668, "Language Common"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 668, "Language Common"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 668, "Language Common");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 668, "Language Common");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 668, "Language Common");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 668, "Language Common");

-- Language Darnassian 

-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 671, "Language Darnassian"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 671, "Language Darnassian");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 671, "Language Darnassian"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 671, "Language Darnassian"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 671, "Language Darnassian");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 671, "Language Darnassian");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 671, "Language Darnassian");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 671, "Language Darnassian");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 671, "Language Darnassian");

-- Language Taurahe
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 670, "Language Taurahe"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 670, "Language Taurahe");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 670, "Language Taurahe"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 670, "Language Taurahe"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 670, "Language Taurahe"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 670, "Language Taurahe");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 670, "Language Taurahe");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 670, "Language Taurahe");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 670, "Language Taurahe");

-- Language Troll
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 7341, "Language Troll"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 7341, "Language Troll");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 7341, "Language Troll"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 7341, "Language Troll"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 7341, "Language Troll"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 7341, "Language Troll");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 7341, "Language Troll");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 7341, "Language Troll");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 7341, "Language Troll");

-- Language Draenei
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 29932, "Language Draenei"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 29932, "Language Draenei");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 29932, "Language Draenei"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 29932, "Language Draenei"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 29932, "Language Draenei");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 29932, "Language Draenei");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 29932, "Language Draenei");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 29932, "Language Draenei");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 29932, "Language Draenei");

-- Language Gutterspeak
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 17737, "Language Gutterspeak"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 17737, "Language Gutterspeak");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 17737, "Language Gutterspeak"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 17737, "Language Gutterspeak"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 17737, "Language Gutterspeak"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 17737, "Language Gutterspeak");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 17737, "Language Gutterspeak");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 17737, "Language Gutterspeak");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 17737, "Language Gutterspeak");


-- Language Gnomish
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 7340, "Language Gnomish"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 7340, "Language Gnomish");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 7340, "Language Gnomish"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 7340, "Language Gnomish"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 7340, "Language Gnomish");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 7340, "Language Gnomish");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 7340, "Language Gnomish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 7340, "Language Gnomish");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 7340, "Language Gnomish");

-- MISC Fixes --
-- Wear Cloth
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 9078, "Cloth"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 9078, "Cloth");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 9078, "Cloth"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 9078, "Cloth"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 9078, "Cloth"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 9078, "Cloth");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 9078, "Cloth");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 9078, "Cloth");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 9078, "Cloth");


-- 1H Sword
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1, 201, "1h Sword"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 8, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 16, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 32, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 128, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 256, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 1024, 201, "1h Sword");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1, 201, "1h Sword"); 		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 8, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 16, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 32, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 128, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 256, 201, "1h Sword"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 1024, 201, "1h Sword"); 

-- Horde Talk

-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1, 668, "1h Sword");		
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 8, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 16, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 32, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 128, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 256, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 1024, 201, "1h Sword");

-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 8, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 16, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 32, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 128, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 256, 201, "1h Sword");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 1024, 201, "1h Sword");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 0x5FF, 201, "1h Sword");


-- Totems For Shammies
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 64, 27763, "Totem");
-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 64, 27763, "Totem"); 
-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 64, 27763, "Totem");
-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 64, 27763, "Totem");
-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 64, 27763, "Totem");

-- Guns Bows and Shoot
-- Hunters
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 266, "Gun");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 264, "bow");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 4, 126354, "shoot");

-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 266, "gun"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 264, "bow"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 4, 126354, "shoot"); 
-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 266, "gun");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 264, "bow");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 4, 126354, "shoot");
-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 266, "gun");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 264, "bow");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 4, 126354, "shoot");

-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 64, 266, "gun");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 64, 264, "bow");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 64, 126354, "shoot");

-- Wear Plate for Pallies
-- Nagas 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2, 750, "Plate");
-- Broken
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(8192, 2, 750, "Plate"); 
-- Vrykul
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(32768, 2, 750, "Plate");
-- Goblin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(256, 2, 750, "Plate");
-- Everyone Else:
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(0x6FF, 2, 750, "Plate");




