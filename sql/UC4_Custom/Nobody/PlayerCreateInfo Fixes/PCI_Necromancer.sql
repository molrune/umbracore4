-- Races 1 and 16 for Human and Undead for Class 12 (2048) Bitmask
-- Add Languages -ALL
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 7341, "Language Troll"); 	
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 7341, "Language Troll");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 29932, "Language Draenei"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 17737, "Language Gutterspeak");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 668, "Language Common"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 668, "Language Common");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 669, "Language Orcish");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 669, "Language Orcish"); 	
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 671, "Language Darnassian"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 671, "Language Darnassian"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 670, "Language Taurahe"); 		 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 670, "Language Taurahe");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 7340, "Language Gnomish"); 
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 7340, "Language Gnomish"); 

-- Wear Cloth

REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 9078, "Cloth");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 9078, "Cloth"); 	

-- Use 1H Sword
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 201, "1h Sword"); 	
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 201, "1h Sword"); 

-- Use Shoot
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 126354, "shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 126354, "shoot");	

-- Unarmed Combat
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 203, "Unarmed");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 203, "Unarmed");	 

-- Defense
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 204, "Defense");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 204, "Defense");

-- Spell Def
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 204, "SPELLDEFENSE DND");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 204, "SPELLDEFENSE DND");

-- Generic?
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 2382, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 2382, "Generic");

-- Honorless target
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 2479, "Honorless Target");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 2479, "Honorless Target");

-- Open
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 3365, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 3365, "Opening");

-- Closing
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 6233, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 6233, "Closing");

-- Closing2
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 6246, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 6246, "Closing");

-- Opening2
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 6247, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 6247, "Opening");

-- Opening3
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 6477, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 6477, "Opening");

-- Opening4
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 6478, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 6478, "Opening");

-- Attack
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 6603, "Attack");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 6603, "Attack");

-- Duel
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 7266, "Duel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 7266, "Duel");

-- Grovel
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 7267, "Grovel");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 7267, "Grovel");

-- Stuck
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 7355, "Stuck");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 7355, "Stuck");

-- Attacking
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 8386, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 8386, "Attacking");

-- Attacking2
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 9077, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 9077, "Attacking");

-- Attacking3
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 9078, "Attacking");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 9078, "Attacking");

-- Generic2
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 9125, "Generic");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 9125, "Generic");

-- Opening
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 21651, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 21651, "Opening");

-- Closing
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 21652, "Closing");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 21652, "Closing");

-- Remove Insignia
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 22027, "Remove Insignia");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 22027, "Remove Insignia");

-- Opening - No Txt
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 22810, "Opening - No Text");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 22810, "Opening - No Text");

-- Victorious State
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 32215, "Victorious State");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 32215, "Victorious State");

-- Summon Friend
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 45927, "Summon Friend");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 45927, "Summon Friend");

-- Opening
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 61437, "Opening");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 61437, "Opening");

-- Daggars
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 1180, "Daggers");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 1180, "Daggers");

-- Defensive State
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 16092, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 16092, "Defensive State (DND)");

-- Wands
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 5009, "Wands");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 5009, "Wands");

-- Shoot
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 5019, "Shoot");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 5019, "Shoot");

-- Linen Bandage
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 3275, "Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 3275, "Linen Bandage");

-- Heavey Linen
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 3276, "Heavy Linen Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 3276, "Heavy Linen Bandage");

-- Wool Bandage
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 3277, "Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 3277, "Wool Bandage");

-- Heavy wool
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 3278, "Heavy Wool Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 3278, "Heavy Wool Bandage");

-- Silk Bandage
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 7928, "Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 7928, "Silk Bandage");

-- Heavy Silk
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 7934, "Heavy Silk Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 7934, "Heavy Silk Bandage");

-- Mageweave Bandage
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 10840, "Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 10840, "Mageweave Bandage");

-- Heavy Mageweave
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 10841, "Heavy Mageweave Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 10841, "Heavy Mageweave Bandage");

-- First Aid
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 10846, "First Aid");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 10846, "First Aid");

-- Runecloth Bandgage
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 18629, "Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 18629, "Runecloth Bandage");

-- Heavy Rcloth
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 18630, "Heavy Runecloth Bandage");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 18630, "Heavy Runecloth Bandage");

-- Journeyman Riding
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 33391, "Journeyman Riding");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(16, 2048, 33391, "Journeyman Riding");

-- OS State DND
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 45903, "Offensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2048, 45903, "Offensive State (DND)");

-- Fireball
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 133, "Fireball");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2048, 133, "Fireball");

-- Frost Armor
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 168, "Frost Armor");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2048, 168, "Frost Armor");

-- Demon Skin
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 687, "Demon Skin");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2048, 687, "Demon Skin");

-- Def State DND
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 13358, "Defensive State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2048, 13358, "Defensive State (DND)");

-- Def State 2
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(1, 2048, 24949, "Defensive State 2 (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 2048, 24949, "Defensive State 2 (DND)");

-- Adv State
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 34082, "Advantaged State (DND)");
REPLACE INTO `playercreateinfo_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES(4096, 0x4, 34082, "Advantaged State (DND)");

-- 







		 
	 

