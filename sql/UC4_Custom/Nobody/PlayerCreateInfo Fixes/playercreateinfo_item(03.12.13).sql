-- DK Start Items
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34652, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34657, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34655, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34659, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34650, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34653, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34649, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34651, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34656, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34648, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 34658, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(0, 6, 38147, 1);

-- Race: Goblin (9)
-- Class: Shaman (7) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 7, 6135, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 7, 6134, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 7, 36, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 7, 2362, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 7, 46978, 1);

-- Race: Naga (13)
-- Class: Shaman (7) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 7, 6135, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 7, 6134, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 7, 36, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 7, 2362, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 7, 46978, 1);

-- Race: Vrykul (16)
-- Class: Shaman (7) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 7, 6135, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 7, 6134, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 7, 36, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 7, 2362, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 7, 46978, 1);

-- Race: Broken (14)
-- Class: Shaman (7) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 7, 6135, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 7, 6134, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 7, 36, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 7, 2362, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 7, 46978, 1);

-- Race: Broken (14)
-- Class: Warrior (1) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 1, 20902, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 1, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 1, 40, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 1, 49778, 1);

-- Race: Naga (13)
-- Class: Warrior (1) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 1, 20902, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 1, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 1, 40, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 1, 49778, 1);

-- Race: Vrykul (16)
-- Class: Warrior (1) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 1, 20902, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 1, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 1, 40, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 1, 49778, 1);

-- Race: Goblin (9)
-- Class: Warrior (1) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 1, 20902, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 1, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 1, 40, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 1, 49778, 1);

-- Race: Goblin (9)
-- Class: Paladin (2) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 2, 43, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 2, 45, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 2, 23477, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 2, 2361, 1);

-- Race: Broken (14)
-- Class: Paladin (2) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 2, 43, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 2, 45, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 2, 23477, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 2, 2361, 1);

-- Race: Naga (13)
-- Class: Paladin (2) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 2, 43, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 2, 45, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 2, 23477, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 2, 2361, 1);

-- Race: Vrykul (16)
-- Class: Paladin (2) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 2, 43, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 2, 45, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 2, 23477, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 2, 2361, 1);

-- Race: Vrykul (16)
-- Class: Hunter (3) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 3, 148, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 3, 147, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 3, 129, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 3, 12282, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 3, 2516, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 3, 2508, 1);

-- Race: Broken (14)
-- Class: Hunter (3) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 3, 148, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 3, 147, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 3, 129, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 3, 12282, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 3, 2516, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 3, 2508, 1);

-- Race: Naga (13)
-- Class: Hunter (3) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 3, 148, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 3, 147, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 3, 129, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 3, 12282, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 3, 2516, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 3, 2508, 1);

-- Race: Goblin (9)
-- Class: Hunter (3) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 3, 148, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 3, 147, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 3, 129, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 3, 12282, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 3, 2516, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 3, 2508, 1);

-- Race: Naga (13)
-- Class: Rogue (4) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 4, 49, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 4, 48, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 4, 47, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 4, 28979, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 4, 2092, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 4, 50055, 1);

-- Race: Broken (14)
-- Class: Rogue (4) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 4, 49, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 4, 48, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 4, 47, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 4, 28979, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 4, 2092, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 4, 50055, 1);

-- Race: Vrykul (16)
-- Class: Rogue (4) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 4, 49, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 4, 48, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 4, 47, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 4, 28979, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 4, 2092, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 4, 50055, 1);

-- Race: Goblin (9)
-- Class: Rogue (4) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 4, 49, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 4, 48, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 4, 47, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 4, 28979, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 4, 2092, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 4, 50055, 1);

-- Race: Vrykul (16)
-- Class: Priest (5) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 5, 53, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 5, 52680, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 5, 6098, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 5, 51, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 5, 35, 1);

-- Race: Naga (13)
-- Class: Priest (5) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 5, 53, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 5, 52680, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 5, 6098, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 5, 51, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 5, 35, 1);

-- Race: Broken (14)
-- Class: Priest (5) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 5, 53, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 5, 52680, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 5, 6098, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 5, 51, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 5, 35, 1);

-- Race: Goblin (9)
-- Class: Priest (5) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 5, 53, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 5, 52680, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 5, 6098, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 5, 51, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 5, 35, 1);

-- Race: Goblin (9)
-- Class: Mage (8) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 8, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 8, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 8, 39, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 8, 23321, 1);

-- Race: Vrykul (16)
-- Class: Mage (8) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 8, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 8, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 8, 39, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 8, 23321, 1);

-- Race: Naga (13)
-- Class: Mage (8) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 8, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 8, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 8, 39, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 8, 23321, 1);

-- Race: Broken (14)
-- Class: Mage (8) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 8, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 8, 38, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 8, 39, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 8, 23321, 1);

-- Race: Broken (14)
-- Class: Warlock (9) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 57, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 6097, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 1396, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 59, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 35, 1);

-- Race: Goblin (9)
-- Class: Warlock (9) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 9, 57, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 9, 6097, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 9, 1396, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 9, 59, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 9, 35, 1);

-- Race: Broken (14)
-- Class: Warlock (9) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 57, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 6097, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 1396, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 59, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 9, 35, 1);

-- Race: Vrykul (16)
-- Class: Warlock (9) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 9, 57, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 9, 6097, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 9, 1396, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 9, 59, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 9, 35, 1);

-- Race: Vrykul (16)
-- Class: Druid (11) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 11, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 11, 6123, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(16, 11, 6124, 1);

-- Race: Broken (14)
-- Class: Druid (11) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 11, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 11, 6123, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(14, 11, 6124, 1);

-- Race: Naga (13)
-- Class: Druid (11) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 11, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 11, 6123, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(13, 11, 6124, 1);

-- Race: Goblin (9)
-- Class: Druid (11) 
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 11, 3661, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 11, 6123, 1);
REPLACE INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`) VALUES(9, 11, 6124, 1);















